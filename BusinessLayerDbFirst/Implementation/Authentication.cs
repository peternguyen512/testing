﻿using System;
using System.Linq;
using BusinessLayerDbFirst.Interface;
using Database;
using Database.Model;

namespace BusinessLayerDbFirst.Implementation
{
    public class Authentication : IAuthentication
    {
        private readonly DatabaseContext _context;
        public Authentication(DatabaseContext context)
        {
            _context = context;
        }

        public User CheckUsernamePassword(string username, string password)
        {
            var user = _context.Users.SingleOrDefault(x => x.Username == username && x.Password == password);
            if(user == null)
            {
                return null;
            }
            return user;
        }
        public User GetUserIdByUsername(string username)
        {
            var user = _context.Users.SingleOrDefault(x => x.Username == username);
            if(user == null)
            {
                return null;
            }
            else
            {
                return user;
            }
        }

    }
}
