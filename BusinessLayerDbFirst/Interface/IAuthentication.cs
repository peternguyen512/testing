﻿using System;
using Database.Model;

namespace BusinessLayerDbFirst.Interface
{
    public interface IAuthentication
    {
        public User CheckUsernamePassword(string username, string password);
        public User GetUserIdByUsername(string username);
    }
}
