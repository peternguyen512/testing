﻿using System;
using System.Linq;
using Database.Model;

namespace Database
{
    public static class DbInitializer
    {
        public static void Initialize(DatabaseContext context)
        {
            context.Database.EnsureCreated();
            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }
            var user = new User()
            {
                Username = "admin",
                Password = "admin",
                Email = "adfaf@email.com"
            };
            context.Users.Add(user);
            context.SaveChanges();
        }
    }
}
