﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.Model
{
    public class User
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Provider { get; set; }
    }
}
