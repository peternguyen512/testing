﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BusinessLayerDbFirst.Interface;
using Database.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebAPI.Middleware;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IAuthentication _authentication;
        private readonly AppSettings _appSettings;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IAuthentication authentication
            , IOptions<AppSettings> appSettings)
        {
            _logger = logger;
            _authentication = authentication;
            _appSettings = appSettings.Value;
        }
        [HttpPost("authenticate")]
        public IActionResult Authenticate(string username, string password)
        {
            //throw new Exception("test ");
            var user = _authentication.CheckUsernamePassword(username, password);
            if (user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            return Ok(generateJwtToken(user));
        }
        private string generateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", user.Username.ToString()) }),
                //Subject = new ClaimsIdentity(new[] { new Claim("username", "peternguyen512") }),

                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        [Authorize]
        [HttpGet]
        public ActionResult Get()
        {
            
            return new JsonResult("You are authenticated!");
        }
    }
}
