﻿using System;
namespace WebAPI.Model.Error
{
    public class HttpException
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
