﻿using System;

namespace Domain
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
    public static class Test1
    {
        public static string ReturnString()
        {
            return "test";
        }
    }
    public class Test2
    {
        public static string Test3()
        {
            return "";
        }
    }
    public class MainClass
    {
        public string Main()
        {
            var a = new Test2();
            Test2.Test3();
            Test1.ReturnString();
            return string.Empty;
        }
    }

}

