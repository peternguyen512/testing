﻿using System;
using System.Threading.Tasks;
using Database.Model;
using Microsoft.AspNetCore.Http;

namespace WebMVC.Provider
{
    public interface IUserManager
    {
        Task SignIn(HttpContext httpContext, User user, bool isPersistent = false);
        Task SignOut(HttpContext httpContext);
    }
}
