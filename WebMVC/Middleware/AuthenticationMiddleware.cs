﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BusinessLayerDbFirst.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebAPI.Middleware;

namespace WebMVC.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;

        public AuthenticationMiddleware(RequestDelegate next
            , IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public Task Invoke(HttpContext httpContext, IAuthentication authentication)
        {

            if (!httpContext.User.Identity.IsAuthenticated)
            {
                var token = httpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

                if (token != null)
                {
                    attachUserToContext(httpContext, token, authentication);
                }
                else // try to get from session
                {
                    token = httpContext.Session.GetString("token");
                    if (token != null)
                    {
                        attachUserToContext(httpContext, token, authentication);
                    }
                    else //try to get from cookies
                    {
                        var cookies = httpContext.Request.Cookies.Where(x => x.Key == "UserInfor").FirstOrDefault();
                        if(cookies.Key != null)
                        {
                            token = cookies.Value;
                            attachUserToContext(httpContext, token, authentication);
                        }
                    }
                }
            }
            
            return _next(httpContext);
        }

        private void attachUserToContext(HttpContext context, string token, IAuthentication authentication)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var username = jwtToken.Claims.First(x => x.Type == "username").Value;

                // attach user to context on successful jwt validation
                var user = authentication.GetUserIdByUsername(username);
                var identity = new ClaimsIdentity(new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Username, ClaimValueTypes.String),
                    new Claim(ClaimTypes.Role, "SuperAdmin", ClaimValueTypes.String),
                }, "JWT");
                context.User = new ClaimsPrincipal(identity);
            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthenticationMiddlewareExtensions
    {
        public static IApplicationBuilder UseMiddlewareAuthentication(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationMiddleware>();
        }
    }
}
