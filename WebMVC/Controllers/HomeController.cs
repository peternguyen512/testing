﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using BusinessLayerDbFirst.Interface;
using Database.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebAPI.Middleware;
using WebMVC.Models;
using Microsoft.AspNetCore.Identity;

namespace WebMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAuthentication _authentication;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly AppSettings _appSettings;
        public HomeController(ILogger<HomeController> logger, IAuthentication authentication
            , IOptions<AppSettings> appSettings)
        {
            _logger = logger;
            _authentication = authentication;
            _appSettings = appSettings.Value;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Privacy()
        {
            return View();
        }
        [Authorize]
        public async System.Threading.Tasks.Task<IActionResult> LogoutAsync()
        {
            HttpContext.Session.Clear();
            //HttpContext..Clear();
           //await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            Response.Cookies.Delete(".AspNetCore.Cookies");
            Response.Cookies.Delete("UserInfor");
            return RedirectToAction("Index");
        }

        [Authorize]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult UnAuthorized()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async System.Threading.Tasks.Task<IActionResult> LoginAsync(string username, string password)
        {
            var user = _authentication.CheckUsernamePassword(username, password);
            if (user == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            var token = generateJwtToken(user);


            var claims = new List<Claim>
            {
                //new Claim(ClaimTypes.Email, user.Email),
                new Claim("FullName", user.Username),
                new Claim(ClaimTypes.Role, "Administrator"),
                //new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Name, user.Username)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, "Custome", "CustomeCookies", "Custom");
            
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.Now.AddDays(1),
                IsPersistent = true,
            };

            //Default asp.net sign in
            //await HttpContext.SignInAsync("CustomeCookies", new ClaimsPrincipal(claimsIdentity), authProperties);

            //Cookies sign in
            HttpContext.Response.Cookies.Append("UserInfor",token,new CookieOptions { Expires = DateTime.UtcNow.AddDays(1) });

            //Session sign in
            //HttpContext.Session.SetString("token", token);

            return RedirectToAction("Privacy");
        }

        private string generateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", user.Username.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
