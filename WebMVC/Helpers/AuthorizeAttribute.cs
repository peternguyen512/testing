﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Database.Model;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using WebMVC.Models;
using System.Diagnostics;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var user = context.HttpContext.User;
        if (!user.Identity.IsAuthenticated)
        {
            // not logged in
            context.Result = new RedirectResult("~/Home/Unauthorized");
        }
    }
}